from flask import g

from vedavaapi.common.helpers.api_helper import get_current_org, get_initial_agents
from vedavaapi.common.helpers.models import VVContext

from . import myservice


def _get_objstore_colln():
    org_name = get_current_org()
    return myservice().colln(org_name)


def _get_data_dir_path():
    org_name = get_current_org()
    return myservice().instance.registry.lookup('objstore').data_dir_path(org_name)


def _get_acl_svc():
    current_org_name = get_current_org()
    acls_service = myservice().registry.lookup('acls')
    return acls_service.get_acl_svc(current_org_name)


def _get_abstract_file_resolver():
    current_org_name = get_current_org()
    abstractfs_svc = myservice().registry.lookup('abstractfs')
    if not abstractfs_svc:
        return None
    # noinspection PyUnresolvedReferences
    from vedavaapi.abstractfs.lib import AbstractFileResolver
    return abstractfs_svc.resolver(current_org_name)  # type: AbstractFileResolver


def _get_schema_validator():
    current_org_name = get_current_org()
    schema_service = myservice().registry.lookup('schemas')
    return schema_service.get_schema_validator(current_org_name, _get_objstore_colln())


def push_environ_to_g():
    g.current_org_name = get_current_org()
    g.objstore_colln = _get_objstore_colln()
    g.data_dir_path = _get_data_dir_path()
    g.acl_svc = _get_acl_svc()
    g.schema_validator = _get_schema_validator()
    g.initial_agents = get_initial_agents()
    g.vv_context = VVContext(
        g.objstore_colln, g.data_dir_path, g.acl_svc, g.schema_validator)
    g.vv_context_descr = g.vv_context.get_descriptor()
    g.abstract_file_resolver = _get_abstract_file_resolver()
