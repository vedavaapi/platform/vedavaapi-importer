import flask_restx
from flask import g
from vedavaapi.acls.permissions_helper import PermissionResolver

from vedavaapi.common.helpers.api_helper import error_response
from vedavaapi.common.helpers.args_parse_helper import parse_json_args
from vedavaapi.common.helpers.token_helper import require_oauth, current_token

from vedavaapi.common.helpers.models import AgentContext
from vedavaapi.common.helpers import celery_helper

from . import api


iiif_importer_ns = api.namespace('iiif_importer', path='/iiif_importer')


def check_known_namespaces(url: str):
    if url.startswith('https://archive.org/') or url.startswith('https://iiif.archivelab.org'):
        return 'archive.org'
    return None


@iiif_importer_ns.route('/import_task')
class ImportTask(flask_restx.Resource):

    post_parser = iiif_importer_ns.parser()
    post_parser.add_argument('library', type=str, location='form', required=True)
    post_parser.add_argument('url', type=str, location='form', required=True)
    post_parser.add_argument('upsert', type=str, location='form', choices=('true', 'false'), default='false')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    post_payload_json_parse_directives = {
        "upsert": {"allowed_types": [bool], "default": False}
    }

    @iiif_importer_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = parse_json_args(self.post_parser.parse_args(), self.post_payload_json_parse_directives)

        library = g.objstore_colln.find_one({"_id": args['library']})
        if not PermissionResolver.resolve_permission(
                library, PermissionResolver.CREATE_CHILDREN,
                current_token.user_id, current_token.team_ids, g.objstore_colln, g.acl_svc):
            return error_response(
                message='permission denied to create children to library {}'.format(args['library']), code=403)

        agent_context = AgentContext(current_token.user_id, current_token.team_ids)

        from ...lib.iiif_importer.worker import book_import_task
        task = book_import_task.delay(
            g.vv_context_descr, agent_context, args['library'], args['url'], args['upsert'])

        task_status_url = api.url_for(CeleryTask, task_id=task.id, _external=True)
        return {"status_url": task_status_url, "task_id": task.id}, 202, {
            'Location': task_status_url, 'Access-Control-Expose-Headers': 'Location'
        }


@iiif_importer_ns.route('/tasks/<task_id>')
class CeleryTask(flask_restx.Resource):

    def get(self, task_id):
        from vedavaapi.common.helpers.celery import celery as celery_app
        return celery_helper.get_task_status(celery_app, task_id)

    def delete(self, task_id):
        from vedavaapi.common.helpers.celery import celery as celery_app
        celery_helper.terminate_task(celery_app, task_id)
        return {"task_id": task_id, "success": True}
