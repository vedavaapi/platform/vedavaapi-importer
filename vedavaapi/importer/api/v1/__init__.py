import os.path

import flask_restx
from flask import Blueprint

from .. import myservice


api_blueprint_v1 = Blueprint(myservice().name + '_v1', __name__)


api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title=myservice().title,
    description="Vedavaapi Importer Api",
    doc='/docs'
)

from . import iiif_importer_ns
from . import pdf_importer_ns
