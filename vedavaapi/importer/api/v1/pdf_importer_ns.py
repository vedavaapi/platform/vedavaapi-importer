import json

import flask_restx
from flask import g
import urllib.parse

from vedavaapi.common.helpers.api_helper import error_response
from vedavaapi.common.helpers.models import AgentContext
from vedavaapi.common.helpers.token_helper import require_oauth, current_token

from vedavaapi.common.helpers import celery_helper
from vedavaapi.objectdb.helpers import ObjModelException
from vedavaapi.objectdb.helpers.objstore_graph_helper import GraphValidationError

from ...lib.pdf import import_pdf_to_book
from ...lib.pdf.worker import import_multiple_items_task
from . import api


pdf_importer_ns = api.namespace('pdf_importer', path='/pdf_importer')


@pdf_importer_ns.route('/import')
class Importer(flask_restx.Resource):

    post_parser = pdf_importer_ns.parser()
    post_parser.add_argument('book_id', type=str, location='form', required=True)
    post_parser.add_argument('pdf_id', type=str, location='form', required=False)
    post_parser.add_argument('pdffs_args', type=str, location='form', required=False)
    post_parser.add_argument(
        'is_scanned_pdf', type=str, location='form',
        choices=["true", "false"], required=False, default='false')
    post_parser.add_argument(
        'set_cover', type=str, location='form',
        choices=["true", "false"], required=False, default='true')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    @pdf_importer_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = self.post_parser.parse_args()
        pdffs_args = None
        if args.pdffs_args:
            try:
                pdffs_args = dict(urllib.parse.parse_qsl(
                    args.pdffs_args, keep_blank_values=False, strict_parsing=True))
            except ValueError:
                return error_response(message='invalid pdffs_args', code=400)

        try:
            return import_pdf_to_book(
                g.vv_context, current_token,
                args.book_id, args.pdf_id, g.abstract_file_resolver,
                is_scanned_pdf=json.loads(args.is_scanned_pdf),
                set_cover=json.loads(args.set_cover),
                pdffs_args=pdffs_args
            )
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        except GraphValidationError as e:
            return error_response(
                message=str(e), code=e.http_status_code, error=str(e.error))


@pdf_importer_ns.route('/multi_import_tasks')
class MultiImportTasks(flask_restx.Resource):

    post_parser = pdf_importer_ns.parser()
    post_parser.add_argument('item_specs', type=str, location='form', required=True)
    post_parser.add_argument('pdffs_args', type=str, location='form', required=False)
    post_parser.add_argument(
        'are_scanned_pdfs', type=str, location='form',
        choices=["true", "false"], required=False, default='false')
    post_parser.add_argument(
        'set_cover', type=str, location='form',
        choices=["true", "false"], required=False, default='true')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    @pdf_importer_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = self.post_parser.parse_args()
        pdffs_args = None
        if args.pdffs_args:
            try:
                pdffs_args = urllib.parse.parse_qsl(
                    args.pdffs_args, keep_blank_values=False, strict_parsing=True)
            except ValueError:
                return error_response(message='invalid pdffs_args', code=400)
        try:
            item_specs = json.loads(args.item_specs)
            if not isinstance(item_specs, list):
                raise ValueError('item_specs must be list of tuples')
            for item in item_specs:
                if not (
                        isinstance(item, str)
                        or (isinstance(item, list) and len(item) == 2)
                ):
                    raise ValueError('invalid item_specs')
        except json.JSONDecodeError:
            return error_response(message=f'invalid json for item_specs', code=400)
        except ValueError as e:
            return error_response(message=str(e), code=400)

        agent_context = AgentContext(current_token.user_id, current_token.team_ids)

        task = import_multiple_items_task.delay(
            g.vv_context_descr, agent_context, item_specs,
            g.abstract_file_resolver.get_descriptor(),
            are_scanned_pdfs=json.loads(args.are_scanned_pdfs),
            pdffs_args=pdffs_args, set_cover=json.loads(args.set_cover)
        )

        task_status_url = api.url_for(CeleryTask, task_id=task.id, _external=True)
        return {"status_url": task_status_url, "task_id": task.id}, 202, {
            'Location': task_status_url, 'Access-Control-Expose-Headers': 'Location'
        }


@pdf_importer_ns.route('/tasks/<task_id>')
class CeleryTask(flask_restx.Resource):

    # noinspection PyMethodMayBeStatic
    def get(self, task_id):
        from vedavaapi.common.helpers.celery import celery as celery_app
        return celery_helper.get_task_status(celery_app, task_id)

    # noinspection PyMethodMayBeStatic
    def delete(self, task_id):
        from vedavaapi.common.helpers.celery import celery as celery_app
        celery_helper.terminate_task(celery_app, task_id)
        return {"task_id": task_id, "success": True}
