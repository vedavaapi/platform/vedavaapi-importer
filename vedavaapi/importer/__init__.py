import six

from vedavaapi.common import VedavaapiService, OrgHandler, unicode_for


class ImporterOrgHandler(OrgHandler):
    def __init__(self, service, org_name):
        super(ImporterOrgHandler, self).__init__(service, org_name)
        self.objstore_colln = self.service.registry.lookup('objstore').colln(self.org_name)


class VedavaapiImporter(VedavaapiService):

    instance = None  # type: VedavaapiImporter

    dependency_services = ['objstore']
    org_handler_class = ImporterOrgHandler

    title = 'Vedavaapi Importer'
    description = "Vedavaapi Importer Api"

    def __init__(self, registry, name, conf):
        super(VedavaapiImporter, self).__init__(registry, name, conf)

    def colln(self, org_name):
        return self.get_org(org_name).objstore_colln
