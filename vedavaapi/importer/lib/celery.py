from vedavaapi.common.helpers import celery_helper


celery = celery_helper.make_celery(
    'vedavaapi.importer.lib.celery:celery',
    [
        'vedavaapi.importer.lib.iiif_importer.worker',
        'vedavaapi.importer.lib.pdf.worker'
    ]
)
