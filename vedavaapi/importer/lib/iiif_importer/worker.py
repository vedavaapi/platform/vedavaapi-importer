from celery.exceptions import Ignore
from vedavaapi.client.iiif_import_helper import IIIFImporter

from vedavaapi.objectdb.helpers import objstore_graph_helper, objstore_helper
from vedavaapi.objectdb.helpers.objstore_graph_helper import GraphValidationError

from vedavaapi.common.helpers.models import VVContext, VVContextDescriptor, AgentContext
from vedavaapi.common.helpers.celery import celery as celery_app


def check_known_namespaces(url: str):
    if url.startswith('https://archive.org/') or url.startswith('https://iiif.archivelab.org'):
        return 'archive.org'
    return None


@celery_app.task(bind=True)
def book_import_task(
        self, vv_context_descriptor: VVContextDescriptor, agent_context: AgentContext,
        library_id, iiif_book_url, upsert):

    vv_context = VVContext.from_descriptor(vv_context_descriptor)
    namespace = check_known_namespaces(iiif_book_url)
    importer = IIIFImporter.get_importer(namespace, update_state=self.update_state)  # type: IIIFImporter

    try:
        graph, ool_data_graph, book_blank_id, page_members = importer.graph_from_book_url(iiif_book_url, library_id)

        graph_ids_to_uids_map, ool_data_graph_ids_to_uids_map = objstore_graph_helper.post_graph_with_ool_data(
            vv_context.colln, vv_context.acl_svc, None, agent_context.user_id, agent_context.team_ids,
            graph, ool_data_graph, {}, vv_context.schema_validator,
            initial_agents=vv_context.initial_agents, upsert=upsert
        )

        sequence_anno = importer.get_sequence_annotation(book_blank_id, graph_ids_to_uids_map, page_members)
        sequence_anno_id = objstore_helper.create_or_update(
            vv_context.colln, vv_context.acl_svc, sequence_anno, agent_context.user_id, agent_context.team_ids,
            vv_context.schema_validator, upsert=upsert, initial_agents=vv_context.initial_agents
        )

        return {
            "book_id": graph_ids_to_uids_map[book_blank_id],
            "sequence_anno_id": sequence_anno_id,
            "total": len(page_members), "current": len(page_members)
        }

    except ValueError as e:
        self.update_state(state='FAILURE', meta={
            "exc_type": "ValueError",
            "exc_message": ['url seems invalid'],
            "code": 400, "error": str(e)
        })
        raise Ignore()

    except GraphValidationError as e:
        self.update_state(state='FAILURE', meta={
            "exc_type": "ValueError",
            "exc_message": [str(e)],
            "code": e.http_status_code, "error": e.error
        })
        raise Ignore()
