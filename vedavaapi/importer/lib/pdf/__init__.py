import math
from typing import Optional
import urllib.parse

from vedavaapi.common.helpers.models import AgentContext, VVContext
from vedavaapi.abstractfs.lib import AbstractFileResolver
from vedavaapi.objectdb.helpers import objstore_graph_helper, ObjModelException
from vedavaapi.acls.permissions_helper import PermissionResolver
from vedavaapi.objectdb.helpers.objstore_graph_helper import GraphValidationError


def _caliculate_optimal_zoom_for_page(page_info):
    if not len(page_info['images']):
        return 1.0
    max_image_width = max([im[2] for im in page_info['images']])
    if page_info['w'] > max_image_width:
        return 1.0
    return max_image_width / page_info['w']


def calculate_optimal_zoom(pages_info):
    return max([_caliculate_optimal_zoom_for_page(pi) for pi in pages_info])


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier + 0.5) / multiplier


def get_dimensions(page_info, args):
    if args.get('image_index') is not None:
        image = page_info['images'][args['image_index']]
        return {"width": image[2], "height": image[3]}
    zoom = args.get('zoom', 1)
    return {
        "width": int(round_half_up(page_info['w'] * zoom)),
        "height": int(round_half_up(page_info['h'] * zoom))
    }


def get_args(
        page_index, page_info,
        is_scanned=False, pdffs_args: Optional[dict] = None, optimal_zoom: float = 1.0):
    args = (pdffs_args or {}).copy()
    if 'image_index' in args:
        del args['image_index']
    args['page'] = page_index

    if is_scanned and len(page_info['images']) == 1:
        args['image_index'] = 0
        args.pop('zoom', None)
    elif args.get('zoom') == 'optimal':
        args['zoom'] = optimal_zoom
    return args


def get_page_and_image(
        book_id, pdf_id, page_index, page_info,
        is_scanned=False, pdffs_args: Optional[dict] = None,
        optimal_zoom: float = 1.0, pages_index_offset=0):
    page = {
        "jsonClass": "ScannedPage", "_id": f'_:p-{page_index}',
        'source': book_id,
        'selector': {
            "jsonClass": "IndexSelector",
            "index": str(page_index + pages_index_offset).zfill(4)
        }
    }
    args = get_args(
        page_index, page_info,
        is_scanned=is_scanned, pdffs_args=pdffs_args, optimal_zoom=optimal_zoom)
    dimensions = get_dimensions(page_info, args)
    image = {
        "jsonClass": "StillImage",
        "_id": f'_:pi-{page_index}',
        "source": f'_VV:{page["_id"]}',
        "namespace": "_abstract",
        "identifier": f'{pdf_id}/pdffs:{urllib.parse.urlencode(args)}',
        "namespaceData": {
            "jsonClass": "OOLDNamespaceData",
            "namespace": "_abstract",  # TODO not mandatory
            "dimensions": dimensions,
            "mimetype": "image/png"
        }
    }
    page_repr = {
        "jsonClass": "StillImageRepresentation",
        "data": f"_OOLD:{image['_id']}",
        "implements": ["iiif_image"],
        "mimetype": "image/png"
    }
    page_repr.update(dimensions)
    page['representations'] = {
        "jsonClass": "DataRepresentations",
        "stillImage": [page_repr],
        "default": "stillImage"
    }
    return page, image


def _post_graph(
        vv_context: VVContext, agent_context: AgentContext,
        graph, oold_graph, upsert=False):
    return objstore_graph_helper.post_graph_with_ool_data(
        vv_context.colln, vv_context.acl_svc, vv_context.data_dir_path,
        agent_context.user_id, agent_context.team_ids,
        graph, oold_graph, {}, vv_context.schema_validator,
        initial_agents=vv_context.initial_agents, upsert=upsert
    )


def add_pages(
        vv_context: VVContext, book: dict, pdf_oold: dict,
        abstractfs_resolver: AbstractFileResolver,
        is_scanned_pdf=False, pdffs_args: Optional[dict] = None, set_cover=True,
        agent_context: Optional[AgentContext] = None):
    pdf_info = abstractfs_resolver.info('pdffs', pdf_oold, agent_context=agent_context)
    if not pdf_info['page_count']:
        return
    pages_info = pdf_info['pages']

    graph = {}
    oold_graph = {}
    seq_members = []
    cover_oold_id = None

    optimal_zoom = calculate_optimal_zoom(pages_info) if not is_scanned_pdf else 1.0

    for i, pi in enumerate(pages_info):  # TODO should support custom page-range?
        page, image = get_page_and_image(
            book['_id'], pdf_oold['_id'], i, pi,  # TODO custom index_offset should be allowed
            is_scanned=is_scanned_pdf, pdffs_args=pdffs_args,
            optimal_zoom=optimal_zoom, pages_index_offset=1
        )
        graph[page['_id']] = page
        oold_graph[image['_id']] = image
        if i == 0:
            cover_oold_id = image['_id']
        seq_members.append({
            "index": page['selector']['index'],
            "resource": page['_id']
        })

    if cover_oold_id and set_cover:
        graph[book['_id']] = {
            "jsonClass": "ScannedBook",
            "_id": book['_id'],
            "cover": {
                "jsonClass": "StillImageRepresentation",
                "data": f"_OOLD:{cover_oold_id}",
                "mimetype": "image/png"
            }
        }

    gids_to_uids_map, oold_gids_to_uids_map = _post_graph(
        vv_context, agent_context,
        graph, oold_graph, upsert=False
    )

    for member in seq_members:
        member['resource'] = gids_to_uids_map[member['resource']]

    seq_anno = {
        "jsonClass": "SequenceAnnotation",
        "canonical": 'default_canvas_sequence',
        "target": book['_id'],
        "body": {
            "sequenceType": "page_sequence",
            "members": seq_members
        }
    }

    seq_gids_to_uids_map, _ = _post_graph(
        vv_context, agent_context, {"_:seq": seq_anno}, {}, upsert=True
    )
    return {
        "book_id": book['_id'],
        "seq_id": seq_gids_to_uids_map['_:seq'],
        "page_count": pdf_info['page_count']
    }


def _resolve_book_and_pdf_repr(
        vv_context: VVContext, book_id: str, pdf_id: Optional[str]):
    if not book_id:
        raise ObjModelException('book_id is required', 400)
    res_ids = [book_id]
    if pdf_id:
        res_ids.append(pdf_id)
    resources = vv_context.colln.find({"_id": {"$in": res_ids}})
    res_graph = dict([(r['_id'], r) for r in resources])

    if book_id not in res_graph:
        raise ObjModelException(f'book {book_id} doesn\'t exists', 400)
    if pdf_id and pdf_id not in res_graph:
        raise ObjModelException(f'pdf {pdf_id} doesn\'t exists', 400)

    book = res_graph[book_id]
    pdf_oold = res_graph.get(pdf_id)

    if not pdf_id:
        still_image_reprs = book.get('representations', {}).get('stillImage', [])
        pdf_repr = None
        for reprn in still_image_reprs:
            if reprn.get('mimetype') == 'application/png':
                pdf_repr = reprn
        if not pdf_repr or not isinstance(pdf_repr.get('data'), str):
            raise ObjModelException(
                f'pdf_id not given, and cannot be determined from book', 400)
        pdf_oold = vv_context.colln.find_one({"_id": pdf_repr['data'][6:]})
        if not pdf_oold:
            raise ObjModelException(f'pdf doesn\'t exists', 404)

    return book, pdf_oold


def import_pdf_to_book(
        vv_context: VVContext, agent_context: AgentContext,
        book_id: str, pdf_id: Optional[str], abstractfs_resolver: AbstractFileResolver,
        is_scanned_pdf=False, pdffs_args: Optional[dict] = None, set_cover=True):
    book, pdf_oold = _resolve_book_and_pdf_repr(vv_context, book_id, pdf_id)

    book_perms, pdf_perms = PermissionResolver.get_resolved_permissions_for_resources(
        vv_context.colln, vv_context.acl_svc, [book, pdf_oold],
        PermissionResolver.ACTIONS, agent_context.user_id, agent_context.team_ids,
    )
    if book_perms and not book_perms.get(PermissionResolver.CREATE_CHILDREN):
        raise ObjModelException('No permission to create children of book', 403)
    if pdf_perms and not pdf_perms.get(PermissionResolver.READ):
        raise ObjModelException('No permission to read pdf', 403)

    return add_pages(
        vv_context, book, pdf_oold,
        abstractfs_resolver, is_scanned_pdf=is_scanned_pdf,
        pdffs_args=pdffs_args, set_cover=set_cover, agent_context=agent_context
    )


def import_multiple_items(
        vv_context: VVContext, agent_context: AgentContext,
        item_specs: list, abstractfs_resolver: AbstractFileResolver,
        are_scanned_pdfs=False, pdffs_args: Optional[dict] = None, set_cover=True,
        update_state=None
):
    responses = []
    succeeded = 0
    failed = 0

    for i, spec in enumerate(item_specs):
        if isinstance(spec, str):
            spec = [spec, None]
        elif not isinstance(spec, list) or len(spec) != 2:
            responses.append({
                "status": "FAILURE",
                "error": {"message": "invalid spec"}
            })
            continue
        book_id, pdf_id = spec

        try:
            result = import_pdf_to_book(
                vv_context, agent_context, book_id, pdf_id,
                abstractfs_resolver, is_scanned_pdf=are_scanned_pdfs,
                pdffs_args=pdffs_args, set_cover=set_cover
            )
            response = {"status": "SUCCESS", "result": result}
            succeeded += 1
        except (ObjModelException, GraphValidationError) as e:
            error = {"message": str(e), "code": e.http_status_code}
            response = {"status": "FAILURE", "error": error}
            failed += 1
        responses.append(response)
        update_state(
            state='PROGRESS',
            meta={
                "status": f"Importing pages", "count": i, "total": len(item_specs),
                "succeeded": succeeded, "failed": failed}
        )
    return {
        "status": "Pdfs importing resolved",
        "count": len(item_specs),
        "total": len(item_specs),
        "succeeded": succeeded,
        "failed": failed,
        "response": responses
    }
