from typing import Optional

from vedavaapi.common.helpers.models import VVContext, VVContextDescriptor, AgentContext
from vedavaapi.abstractfs.lib import AbstractFileResolver

from vedavaapi.common.helpers.celery import celery as celery_app
from . import import_multiple_items


@celery_app.task(bind=True)
def import_multiple_items_task(
        self, vv_context_descriptor: VVContextDescriptor, agent_context: AgentContext,
        item_specs: list, abstractfs_resolver_descriptor: dict,
        are_scanned_pdfs=False, pdffs_args: Optional[dict] = None, set_cover=True,
):
    vv_context = VVContext.from_descriptor(vv_context_descriptor)
    abstractfs_resolver = AbstractFileResolver.from_descriptor(
        vv_context, abstractfs_resolver_descriptor)
    return import_multiple_items(
        vv_context, agent_context,
        item_specs, abstractfs_resolver,
        are_scanned_pdfs=are_scanned_pdfs, pdffs_args=pdffs_args,
        set_cover=set_cover, update_state=self.update_state
    )
